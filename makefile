help:
	$(info Available Targets)
	$(info - Testing -----------------------------------------------------------------------------------------------------)
	$(info docker_run               | Build and run docker container locally

docker_run:
	docker build -t registry.gitlab.com/wombat9000/firestore-emulator-ci -f Dockerfile.node-jdk .
	docker run -it registry.gitlab.com/wombat9000/firestore-emulator-ci /bin/bash

docker_push:
	docker build -t registry.gitlab.com/wombat9000/firestore-emulator-ci -f Dockerfile.node-jdk .
	docker push registry.gitlab.com/wombat9000/firestore-emulator-ci

